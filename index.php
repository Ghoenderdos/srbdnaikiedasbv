<?php
//  check if url is https and if not redirect to https
    if($_SERVER["HTTPS"] != "on")
    {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }

    $user = $pass = $error = '';
//  add session to login page
    session_start();
    if (isset($_POST['submit']) && !empty($_POST['submit'])) {
        $user = $_POST['user'];
        $pass = $_POST['pass'];
        if ($user === 'admin' && $pass === 'Alfa1') {
//          set user session on succes full login
            $_SESSION['user'] = $user;
            header('Location: /srdb.php');
            exit;
        } else {
            $error = '<div class="errors">Login failed wrong user credentials</div>';
        }
    }
?>
<!DOCTYPE html>
<html class="no-js regex-off">
<head>
    <title>Search &amp; Replace DataBase</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="/style.css">
</head>
<body>

<form action="/" method="post">
    <!-- 0. login -->
    <fieldset class="row row-login">
        <h1>login</h1>
        <?php echo $error; ?>
        <div class="fields fields-small">
            <div class="field field-short">
                <label for="user">user</label>
                <input id="user" name="user" type="text" value="<?php echo $user; ?>"/>
            </div>
            <div class="field field-short">
                <label for="pass">pass</label>
                <input id="pass" name="pass" type="password" value=""/>
            </div>
        </div>

        <div class="fields">
            <span class="submit-group">
                <input type="submit" name="submit[login]" value="login"/>
            </span>
        </div>
    </fieldset>
</form>
</body>
</html>